//
//  TrackCell.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit

class TrackCell: UITableViewCell {

    @IBOutlet private weak var txtTitle: UILabel!
    @IBOutlet private weak var txtSubTitle: UILabel!
    @IBOutlet private weak var txtTime: UILabel!
    @IBOutlet weak var txtTrackNumber: UILabel!
    @IBOutlet weak var playConstraint: NSLayoutConstraint!
    var playing: Bool = false {
        didSet {
            set(playing: playing)
        }
    }

//    override func awakeFromNib() {
//        super.awakeFromNib()
//        set(playing: playing)
//    }

    func configure(withModel model: MusicTrackModel) {
        txtTitle.text = model.title
        txtSubTitle.text = ""
//        txtTime.text = ""
    }

    private func set(playing: Bool) {
        playConstraint.constant = playing ? 24 : 0
    }

}
