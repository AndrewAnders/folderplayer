//
//  TrackSection.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit

class TrackSection: UITableViewCell {

    @IBOutlet private weak var txtTitle: UILabel!
    @IBOutlet private weak var txtCount: UILabel!

    func configure(title: String, count: String) {
        txtTitle.text = "📂 \(title)"
        txtCount.text = count
    }

}
