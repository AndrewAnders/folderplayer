//
//  PlaylistView.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 10.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//
// swiftlint:disable force_cast

import UIKit
import Combine

class PlaylistView: BaseVC<PlaylistViewModel> {

    @IBOutlet private weak var _searchBar: UISearchBar!
    @IBOutlet private weak var _tableView: UITableView!
    @IBOutlet private weak var _buttonPlay: UIButton!

    private var _disposeBag: Set<AnyCancellable> = Set()
    private var _appearFirstTime = true
    private let _refreshControl = UIRefreshControl()
    private var _folders: [Folder] = []
    private var _colapsedSections: [Int] = [] {
        didSet {
            Preferences.collapsedSections = _colapsedSections
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        _colapsedSections = Preferences.collapsedSections
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.refreshControl = _refreshControl
        _refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        _searchBar.delegate = self

        _buttonPlay.layer.cornerRadius = _buttonPlay.bounds.size.width / 2
        _buttonPlay.addTarget(self, action: #selector(playPause), for: .touchUpInside)

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Locate", style: .plain, target: self, action: #selector(scrollToCurrentTrack))

        viewModel?.folders.sink { [weak self] in
            self?._folders = $0
            self?._tableView.reloadData()
        }.store(in: &_disposeBag)

        viewModel?.playerManager.playlist.currentTrack.sink { [weak self] in
            self?._tableView.reloadData()
            if let track = $0 {
                let ipath = self?.viewModel?.playerManager.playlist.indexPath(forTrack: track)
                self?._tableView.selectRow(at: ipath, animated: true, scrollPosition: .none)
            }
        }.store(in: &_disposeBag)

        viewModel?.playerManager.state.sink { [weak self] in
            self?._buttonPlay.setImage(UIImage(systemName: ($0 == .playing) ? "pause" : "playpause"), for: .normal)
        }.store(in: &_disposeBag)
    }

    @objc private func scrollToCurrentTrack() {
        scrollTableToCurrentTrack(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        if _appearFirstTime {
            _appearFirstTime = false
            scrollTableToCurrentTrack(animated: false)
        }
        _tableView.reloadData()
    }
    private func scrollTableToCurrentTrack(animated: Bool) {
        guard let viewModel = viewModel else { return }

        var indexPath = IndexPath(item: 0, section: 0)
        if let track = viewModel.playerManager.playlist.currentTrack.value {
            indexPath = viewModel.playerManager.playlist.indexPath(forTrack: track)
            if let index = _colapsedSections.firstIndex(of: indexPath.section) {
                _colapsedSections.remove(at: index)
                _tableView.reloadData()
            }
        }

        if viewModel.playerManager.playlist.track(atIndexPath: indexPath) != nil {
            guard _tableView.numberOfRows(inSection: indexPath.section) > 0 else { return }
            _tableView.selectRow(at: indexPath, animated: animated, scrollPosition: .middle)
        }
    }

    @objc private func refreshTableView() {
        viewModel?.reload()
        _refreshControl.endRefreshing()
    }
    @objc private func playPause() {
        viewModel?.playPause()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        _searchBar.endEditing(true)
    }
}

extension PlaylistView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        _folders.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _colapsedSections.contains(section) { return 0 }
        return _folders[section].items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackCell.identifier()) as! TrackCell
        let track = _folders[indexPath.section].items[indexPath.row]
        cell.configure(withModel: track)

        if let track = viewModel!.playerManager.playlist.currentTrack.value {
            let playingIP = viewModel!.playerManager.playlist.indexPath(forTrack: track)
            cell.playing = (indexPath == playingIP)
        } else {
            cell.playing = false
        }

        cell.txtTrackNumber.text = String(indexPath.row + 1)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel?.itemSelected(indexPath: indexPath)
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            _tableView.beginUpdates()
            viewModel?.removeItem(indexPath)
            _tableView.deleteRows(at: [indexPath], with: .automatic)
            _tableView.endUpdates()
        }
    }
}

extension PlaylistView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        _folders.count == 1 ? .zero : 28
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: TrackSection.identifier()) as! TrackSection
        let folder = _folders[section]
        header.configure(title: folder.name, count: folder.items.count.description)
        header.tag = section
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerTapped(_:)))
        header.addGestureRecognizer(tapRecognizer)

        return header
    }
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let tag = sender?.view?.tag else { return }
        print("Tapped = \(tag)")

        if let index = _colapsedSections.firstIndex(of: tag) {
            _colapsedSections.remove(at: index)
        } else {
            _colapsedSections.append(tag)
        }
        _tableView.reloadData()
    }
}

extension PlaylistView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel?.search(text: searchText.isEmpty ? nil : searchText)
    }
}
