//
//  ViewController.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//
// swiftlint:disable force_cast

import UIKit
import RxSwift
import RxDataSources

class PlaybackViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var txtTimeCurrent: UILabel!
    @IBOutlet weak var txtTimeTotal: UILabel!

    @IBAction func playPauseButtonAction(_ sender: Any) {
        viewModel?.playPause()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        viewModel?.playerController.playNext()
    }
    @IBAction func prevButtonAction(_ sender: Any) {
        viewModel?.playerController.playPrev()
    }

    let disposeBag = DisposeBag()
    var viewModel: PlaybackViewModel?
    var dataSource: RxTableViewSectionedAnimatedDataSource<Folder>?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PlaybackViewModel()
        guard let viewModel = viewModel else { return }

        dataSource = RxTableViewSectionedAnimatedDataSource<Folder>(
            configureCell: { _, tableView, _, musicTrackModel in
                let cell = tableView.dequeueReusableCell(withIdentifier: TrackCell.identifier()) as! TrackCell
                cell.configure(withModel: musicTrackModel)
                return cell
            },
            titleForHeaderInSection: { dataSource, index in
                return dataSource.sectionModels[index].name
            }
        )
        dataSource?.canEditRowAtIndexPath = { _ in
          return true
        }

        viewModel.folders
            .bind(to: tableView.rx.items(dataSource: dataSource!))
            .disposed(by: disposeBag)

        tableView.rx.itemSelected.subscribe(onNext: {
            self.viewModel?.itemSelected(indexPath: $0)
            }).disposed(by: disposeBag)

        tableView.rx.itemDeleted.subscribe(onNext: {
            viewModel.removeItem($0)
            }).disposed(by: disposeBag)

        viewModel.timings.subscribe(onNext: {
            self.txtTimeCurrent.text = $0.toString()
            self.txtTimeTotal.text = $1.toString()
            self.slider.maximumValue = Float($1)
            self.slider.value = Float($0)
        }).disposed(by: disposeBag)

        slider.rx.value.debounce(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { value in
            self.viewModel?.setProgress(Double(value))
        }).disposed(by: disposeBag)

        viewModel.state
            .map { $0 == .playing }
            .subscribe(onNext: {
            self.playPauseButton.setTitle($0 ? "⏸" : "▶️", for: .normal)
            }).disposed(by: disposeBag)

        viewModel.currentTrackIndex.subscribe(onNext: {
            print($0)
//            if self.tableView.indexPathForSelectedRow != $0 {
                self.tableView.selectRow(at: $0, animated: true, scrollPosition: UITableView.ScrollPosition.none)
//            }
            }).disposed(by: disposeBag)
    }

}
