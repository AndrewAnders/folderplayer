//
//  PlaylistViewModel.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 10.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import Combine

class PlaylistViewModel: PViewModel {
    var coordinator: Coordinator

    var folders = CurrentValueSubject<[Folder], Never>([])
    let playerManager = PlaybackManager.shared
//    var state: CurrentValueSubject<Playback, Never> { playerManager.state }

    required init(coordinator: Coordinator) {
        self.coordinator = coordinator
        folders.send(playerManager.playlist.folders)
    }
    func itemSelected(indexPath: IndexPath) {
        guard let track = playerManager.playlist.track(atIndexPath: indexPath) else { return }
        playerManager.playTrack(track)
    }
    func playPause() {
        playerManager.playPause()
    }
    func removeItem(_ indexPath: IndexPath) {
        playerManager.removeTrack(atIndexPath: indexPath)
        folders.send(playerManager.playlist.folders)
    }
    func reload() {
        playerManager.playlist.loadTracks()
        folders.send(playerManager.playlist.folders)
    }
    func search(text: String?) {
        playerManager.search(text: text)
        folders.send(playerManager.playlist.folders)
    }
}
