//
//  PlayerMainViewModel.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import Combine

class PlaybackViewModel: PViewModel {
    var coordinator: Coordinator

    var folders = CurrentValueSubject<[Folder], Never>([])
    var shuffle: CurrentValueSubject<Bool, Never> { playerManager.playlist.shuffle }
    let playerManager = PlaybackManager.shared
    private var _disposeBag: Set<AnyCancellable> = Set()

    required init(coordinator: Coordinator) {
        self.coordinator = coordinator
        // FIX: make tracks load async, after App loaded
        playerManager.prepare()
        folders.send(playerManager.playlist.folders)
    }

    func playPause() {
        playerManager.playPause()
    }
    func setProgress(_ time: Double) {
        playerManager.setTime(time)
    }
    func showPlaylist() {
        coordinator.showPlaylist()
    }
    func showLog() {
        coordinator.showLog()
    }
    func set(shuffle: Bool) {
        playerManager.playlist.set(shuffle: shuffle)
    }

}
