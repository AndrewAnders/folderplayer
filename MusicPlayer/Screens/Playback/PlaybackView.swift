//
//  PlaybackView.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit
import Combine

class PlaybackView: BaseVC<PlaybackViewModel> {

    @IBOutlet private weak var playPauseButton: UIButton!
    @IBOutlet private weak var prevButton: UIButton!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var _btnShuffle: UIButton!
    @IBOutlet private weak var _playPauseIcon: UIImageView!
    @IBOutlet private weak var _slider: UISlider!

    @IBOutlet private weak var txtTrackTitle: UILabel!
    @IBOutlet private weak var txtArtist: UILabel!
    @IBOutlet private weak var txtTimeCurrent: UILabel!
    @IBOutlet private weak var txtTimeTotal: UILabel!
    @IBOutlet private weak var coverImage: UIImageView!

    @IBAction func playPauseButtonAction(_ sender: Any) {
        viewModel?.playPause()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        viewModel?.playerManager.playNext()
    }
    @IBAction func prevButtonAction(_ sender: Any) {
        viewModel?.playerManager.playPrev()
    }
    @IBAction func logTapped() {
        viewModel?.showLog()
    }

    private var _sliderMoving = false
    private var _disposeBag: Set<AnyCancellable> = Set()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Folder Player"
        coverImage.layer.cornerRadius = 15
        configureButton(playPauseButton)
        configureButton(prevButton)
        configureButton(nextButton)

        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleDeleteTapped))
        doubleTap.numberOfTapsRequired = 2
        deleteButton.addGestureRecognizer(doubleTap)

        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenSwiped))
        edgePan.edges = .right
        view.addGestureRecognizer(edgePan)

        _btnShuffle.addTarget(self, action: #selector(shuffleTap), for: .touchUpInside)

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .organize, target: self, action: #selector(openPlaylist))
        bindings()
    }

    @objc func doubleDeleteTapped() {
        viewModel?.playerManager.removeCurrentTrack()
    }
    @objc func shuffleTap() {
        guard let viewModel = viewModel else { return }
        viewModel.set(shuffle: !viewModel.shuffle.value)
    }
    @objc func screenSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            viewModel?.showPlaylist()
        }
    }
    @objc func onSliderChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began: _sliderMoving = true
            case .ended:
                self.viewModel?.setProgress(Double(self._slider.value))
                _sliderMoving = false
            default: break
            }
        }
    }
    @objc func openPlaylist() {
        viewModel?.showPlaylist()
    }

    func configureButton(_ button: UIButton) {
        button.layer.cornerRadius = 15
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.systemBlue.cgColor
    }

    func bindings() {
        guard let viewModel = viewModel else { return }
        viewModel.playerManager.trackInfo.sink {
            self.txtTrackTitle.text = $0.title
            self.txtArtist.text = $0.artist
            if let image = $0.image {
                self.coverImage.image = image
            } else {
                self.coverImage.image = UIImage(named: "defaultCover")
            }
        }.store(in: &_disposeBag)

        viewModel.playerManager.state.sink {
            self._playPauseIcon.image = UIImage(systemName: ($0 == .playing) ? "pause" : "play")
        }.store(in: &_disposeBag)

        viewModel.playerManager.timings.sink {
            self.txtTimeCurrent.text = $0.toString()
            self.txtTimeTotal.text = $1.toString()
            guard !self._sliderMoving else { return }
            self._slider.maximumValue = Float($1)
            self._slider.value = Float($0)
            let percent = $0/$1
            if percent < 0.1 || percent > 0.9 {
                Logger.shared.log("Timing: \($0.toString())")
            }
        }.store(in: &_disposeBag)

        viewModel.shuffle.sink { [weak self] in
            self?._btnShuffle.alpha = $0 ? 1 : 0.4
        }.store(in: &_disposeBag)

        _slider.addTarget(self, action: #selector(onSliderChanged(slider:event:)), for: .valueChanged)
    }

}
