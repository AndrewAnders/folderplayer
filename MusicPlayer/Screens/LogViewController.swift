//
//  LogViewController.swift
//  MusicPlayer
//
//  Created by Savitskiy on 01.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import UIKit

class LogViewController: UIViewController, Storyborded {

    @IBOutlet weak private var _textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        _textView.text = Logger.shared.getLog()
    }

}
