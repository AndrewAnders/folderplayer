//
//  Logger.swift
//  MusicPlayer
//
//  Created by Savitskiy on 03.11.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

class Logger {
    static let shared = Logger()

    private let _formatter = DateFormatter()
    private var _log: [String] = []
    private init() {}

    func log(_ text: String) {
        let str = "\(time()) \(text)"
        debugPrint("=== " + str)
        guard Constants.Logging else { return }
        _log.append(str)
    }

    func getLog() -> String {
        _log.joined(separator: "\n")
    }

    private func time() -> String {
        _formatter.dateFormat = "HH:mm:ss"
        return _formatter.string(from: Date())
    }
}
