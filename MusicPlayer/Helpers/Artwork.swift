//
//  Artwork.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import UIKit
import MediaPlayer

class Artwork {

    static func from(image: UIImage?) -> MPMediaItemArtwork? {
        let imgArtwork = (image != nil) ? image : UIImage(named: "defaultCover")
        guard let img = imgArtwork else { return nil }
        return MPMediaItemArtwork(boundsSize: img.size, requestHandler: { _ in return img })
    }

}
