//
//  TimeInterval+toString.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 17.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit

extension TimeInterval {

    func toString() -> String {
        let seconds = Int(self)
        let minutes = seconds / 60
        let hours = minutes / 60

        let secondsRemain = seconds - (minutes * 60)

        if hours > 0 {
            return String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, secondsRemain)
        } else {
            return String(format: "%0.2d:%0.2d", minutes, secondsRemain)
        }
    }

}
