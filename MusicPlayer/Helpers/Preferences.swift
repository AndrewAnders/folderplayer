//
//  Preferences.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 22.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

class Preferences {
    private static let kCurrentTrackPath = "currentTrackPath"
    private static let kCollapsedSections = "collapsedSections"
    private static let kShuffle = "kShuffle"

    static var currentTrackPath: String? {
        get {
            let defaults = UserDefaults.standard
            return defaults.string(forKey: Preferences.kCurrentTrackPath)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: Preferences.kCurrentTrackPath)
            defaults.synchronize()
        }
    }

    static var collapsedSections: [Int] {
        get {
            let defaults = UserDefaults.standard
            guard let array = defaults.object(forKey: Preferences.kCollapsedSections) as? [Int] else { return [] }
            return array
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: Preferences.kCollapsedSections)
            defaults.synchronize()
        }
    }

    static var shuffle: Bool {
        get {
            let defaults = UserDefaults.standard
            return defaults.bool(forKey: Preferences.kShuffle)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: Preferences.kShuffle)
            defaults.synchronize()
        }
    }

}
