//
//  UITableViewCell+Identifire.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit

protocol Identifible {
    static func identifier() -> String
}

extension Identifible {
    static func identifier() -> String {
        String(describing: self)
    }
}

extension UITableViewCell: Identifible { }
extension UITableViewHeaderFooterView: Identifible { }
