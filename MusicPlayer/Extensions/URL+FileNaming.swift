//
//  URL+FileNaming.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

extension URL {
    func fileName() -> String {
        return self.deletingPathExtension().lastPathComponent
    }

    func fileExtension() -> String {
        return self.pathExtension
    }

    var isDirectory: Bool {
        return (try? resourceValues(forKeys: [.isDirectoryKey]))?.isDirectory == true
    }
}
