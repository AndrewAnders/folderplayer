//
//  ID3TagsManager.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 27.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit
import ID3TagEditor

struct FileMetaData {
    let title: String
    let artist: String?
    let album: String?
    let picture: UIImage?

    internal init(title: String, artist: String? = nil, album: String? = nil, picture: UIImage? = nil) {
        self.artist = artist
        self.title = title
        self.album = album
        self.picture = picture
    }
//    internal init() {
//        self.title = ""
//        self.artist = nil
//        self.album = nil
//        self.picture = nil
//    }
}

class ID3TagManager {

    func tags(forFile path: String, fileName: String) -> FileMetaData {
        do {
            let id3TagEditor = ID3TagEditor()

            if let id3Tag = try id3TagEditor.read(from: path) {
                let title = (id3Tag.frames[.title] as? ID3FrameWithStringContent)?.content ?? fileName
                let artist = (id3Tag.frames[.artist] as? ID3FrameWithStringContent)?.content
                let album = (id3Tag.frames[.album] as? ID3FrameWithStringContent)?.content
                let picture = searchImage(in: id3Tag)
                return FileMetaData(title: title, artist: artist, album: album, picture: picture)
            }
        } catch {
            debugPrint(error)
        }
        return FileMetaData(title: fileName)
    }

    private func searchImage(in tags: ID3Tag) -> UIImage? {
        var resultImage: UIImage?
        ID3PictureType.allCases.forEach {
            if let imgData = tags.frames[.attachedPicture($0)] as? ID3FrameAttachedPicture {
                resultImage = UIImage(data: imgData.picture)
            }
        }
        return resultImage
    }

}
