//
//  PlaylistManager.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import Combine

class PlaylistManager: PPlaylistManager {

    var currentTrack = CurrentValueSubject<MusicTrackModel?, Never>(nil)
    var shuffle = CurrentValueSubject<Bool, Never>(Preferences.shuffle)

    let fileManager: PMusicFileManager = MusicFileManager()

    var playbackRepeat = true
    var folders = [Folder]() // curent layout

    var shuffledPlaylist: [MusicTrackModel] = []
    private var trackIndexPath = IndexPath(item: 0, section: 0)
    private var shuffledIndex: Int = 0
    private var _foldersBase = [Folder]()

    // MARK: - Public methods
    func set(shuffle: Bool) {
        self.shuffle.send(shuffle)
        Preferences.shuffle = shuffle
        if shuffle {
            shuffleTracks()
        }
    }
    func search(text: String?) {
        guard let text = text else {
            folders = _foldersBase
            return
        }
        let filtered = _foldersBase.compactMap { folder -> Folder? in
            let items = folder.items.compactMap {
                $0.match(search: text) ? $0 : nil
            }
            guard items.count > 0 else { return nil }
            return Folder(name: folder.name, items: items)
        }
        folders = filtered
    }

    func loadTracks() {
        _foldersBase.removeAll()
        fileManager.getMusicDictionary().sorted { $0.key < $1.key }.forEach {
            let tracks = $0.value.sorted {
                $0.fileName() < $1.fileName()
            }.map({
                MusicTrackModel(title: $0.fileName(), url: $0, shortPath: fileManager.getShortPath(url: $0))
            })
            _foldersBase.append(Folder(name: $0.key, items: tracks))
        }
        folders = _foldersBase
        if Preferences.shuffle {
            shuffleTracks()
        }
        if let track = currentTrack.value {
            trackIndexPath = indexPath(forTrack: track)
        }
    }
    // should launch when music loaded
    func restoreCurrentTrack() {
        if let trackPath = Preferences.currentTrackPath, fileManager.exists(file: trackPath) {
            currentTrack.send(track(forPath: trackPath))
            return
        }
        currentTrack.send(nextTrack())
    }
    func nextTrack() -> MusicTrackModel? {
        if folders.count == 0 { return nil }

        if shuffle.value {
            shuffledIndex += 1
            if shuffledIndex >= shuffledPlaylist.count { shuffledIndex = 0 }
            return shuffledPlaylist[shuffledIndex]
        }

        let nextTrack = trackIndexPath.item + 1
        if nextTrack > currentFolder().items.count - 1 {
            guard let folder = nextFolder() else { return nil }
            return folder.items.first
        }
        return currentFolder().items[nextTrack]
    }
    func removeCurrentTrack() {
        removeItem(atIndexPath: trackIndexPath)
    }
    func removeItem(atIndexPath indexPath: IndexPath) {
        guard let current = getCurrentTrack(), let trackToRemove = track(atIndexPath: indexPath) else { return }
        let folder = folders[indexPath.section]
        var tracks = folder.items
        tracks.remove(at: indexPath.item)
        folders[indexPath.section] = Folder(name: folder.name, items: tracks)
        if trackIndexPath.section == indexPath.section {
            let ipath = IndexPath(item: tracks.firstIndex(of: current) ?? 0, section: indexPath.section)
            trackIndexPath = ipath
        }
        fileManager.remove(file: trackToRemove.url)
    }
    func setCurrentTrack(track: MusicTrackModel) {
        currentTrack.send(track)
        Preferences.currentTrackPath = track.shortPath
        trackIndexPath = indexPath(forTrack: track)
    }
    func track(atIndexPath indexPath: IndexPath) -> MusicTrackModel? {
        if trackExists(at: indexPath) {
            return folders[indexPath.section].items[indexPath.item]
        } else {
            let zeroIdexPath = IndexPath(item: 0, section: 0)
            guard trackExists(at: zeroIdexPath) else { return nil }
            return folders[zeroIdexPath.section].items[zeroIdexPath.item]
        }
    }
    func track(forPath shortPath: String) -> MusicTrackModel? {
        for index in 0...folders.count - 1 {
            if let track = folders[index].items.first(where: { $0.shortPath == shortPath }) {
                return track
            }
        }
        return nil
    }
    func indexPath(forTrack track: MusicTrackModel) -> IndexPath {
        for folderIndex in 0...folders.count - 1 {
            if folders[folderIndex].items.contains(track) {
                let itemIndex = folders[folderIndex].items.firstIndex(of: track)
                return IndexPath(item: itemIndex ?? 0, section: folderIndex)
            }
        }
        return IndexPath(item: 0, section: 0)
    }
    func indexPath(forPath shortPath: String) -> IndexPath {
        if let track = track(forPath: shortPath) {
            return indexPath(forTrack: track)
        }
        return IndexPath(item: 0, section: 0)
    }

    func prevTrack() -> MusicTrackModel? {
        if folders.count == 0 { return nil }

        if shuffle.value {
            shuffledIndex -= 1
            if shuffledIndex <= 0 { shuffledIndex = shuffledPlaylist.count - 1 }
            return shuffledPlaylist[shuffledIndex]
        }

        if trackIndexPath.row > 0 {
            return currentFolder().items[trackIndexPath.row - 1]
        }
        guard let folder = prevFolder() else { return nil }
        return folder.items.last
    }

    // MARK: - Private methods
    private func trackExists(at indexPath: IndexPath) -> Bool {
        indexPath.section <= (folders.count - 1) && indexPath.item <= (folders[indexPath.section].items.count - 1)
    }
    private func getCurrentTrack() -> MusicTrackModel? {
        return track(atIndexPath: trackIndexPath)
    }
    private func tracksList(shuffle: Bool = false) -> [MusicTrackModel] {
        var list: [MusicTrackModel] = []
        _foldersBase.forEach { folder in
            folder.items.forEach {
                list.append($0)
            }
        }
        if shuffle {
            return list.shuffled()
        }
        return list
    }
    private func shuffleTracks() {
        shuffledIndex = 0
        shuffledPlaylist = tracksList(shuffle: true)
    }
}

// MARK: - Folder
private extension PlaylistManager {
    func currentFolder() -> Folder {
        folders[trackIndexPath.section]
    }

    func nextFolder() -> Folder? {
        var nextIndex = trackIndexPath.section + 1
        if nextIndex > folders.count - 1 {
            if playbackRepeat {
                nextIndex = 0
            } else {
                return nil
            }
        }
        return folders[nextIndex]
    }

    func prevFolder() -> Folder? {
        if trackIndexPath.section > 0 {
            return folders[trackIndexPath.section - 1]
        }
        if playbackRepeat {
            return folders.last
        } else {
            return nil
        }
    }
}
