//
//  SharedTrackInfoManager.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import MediaPlayer

class SharedTrackInfoManager {

    private var _nowPlayingInfo = [String: Any]()

    func updateInfo(with data: FileMetaData) {
        _nowPlayingInfo[MPMediaItemPropertyTitle] = data.title
        _nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = data.album
        _nowPlayingInfo[MPMediaItemPropertyArtist] = data.artist
        _nowPlayingInfo[MPMediaItemPropertyArtwork] = Artwork.from(image: data.picture)
        MPNowPlayingInfoCenter.default().nowPlayingInfo = _nowPlayingInfo
    }

    func updateTiming(current: Double, total: Double) {
        _nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = current
        _nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = total
        MPNowPlayingInfoCenter.default().nowPlayingInfo = _nowPlayingInfo
    }

}
