//
//  MusicTrackModel.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

struct MusicTrackModel: Equatable, Identifible {
    typealias Identity = String
    var identity: Identity { return title }

    let title: String
    let url: URL
    let shortPath: String

    func match(search text: String) -> Bool {
        title.lowercased().contains(text.lowercased())
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.url.absoluteString == rhs.url.absoluteString
    }
}
