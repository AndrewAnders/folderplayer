//
//  FolderModel.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 06.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

struct Folder {
    var name: String
    var items: [Item]
}

extension Folder {
    typealias Item = MusicTrackModel
    var identity: String { return name }

    init(original: Folder, items: [Item]) {
        self = original
        self.items = items
    }
}

extension Folder: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.name == rhs.name
    }
}
