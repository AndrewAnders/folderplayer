//
//  SystemPlaybackHandler.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import MediaPlayer

class SystemPlaybackHandler {

    var playerController: PlayerManagerProtocol?

    func setup(withPlayer player: PlayerManagerProtocol) {
        let com = MPRemoteCommandCenter.shared()

        com.playCommand.addTarget { _ in
            player.play(fade: false)
            Logger.shared.log("=== com.playCommand")
            return .success
        }
        com.pauseCommand.addTarget { _ in
            player.pause()
            Logger.shared.log("=== com.pauseCommand")
            return .success
        }
        com.togglePlayPauseCommand.addTarget { _ in
            player.playPause()
            Logger.shared.log("=== com.togglePlayPauseCommand")
            return .success
        }
        com.nextTrackCommand.addTarget { _ in
            player.playNext()
            Logger.shared.log("=== com.nextTrackCommand")
            return .success
        }
        com.previousTrackCommand.addTarget { _ in
            player.playPrev()
            Logger.shared.log("=== com.previousTrackCommand")
            return .success
        }
        com.dislikeCommand.addTarget { _ in
            player.removeCurrentTrack()
            Logger.shared.log("=== com.dislikeCommand")
            return .success
        }
        com.changePlaybackPositionCommand.addTarget { event in
            guard let event = event as? MPChangePlaybackPositionCommandEvent else { return .commandFailed }
            player.setTime(event.positionTime)
            return .success
        }
    }
}
