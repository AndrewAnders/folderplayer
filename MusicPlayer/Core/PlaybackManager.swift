//
//  PlayerController.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import Combine
import UIKit

class PlaybackManager {

    var playlist: PPlaylistManager = PlaylistManager()
    var state: CurrentValueSubject<Playback, Never> { _audioPlayerService.state }
    var timings: CurrentValueSubject<(current: Double, total: Double), Never> { _audioPlayerService.timing }
    let trackInfo = PassthroughSubject<(title: String, artist: String?, image: UIImage?), Never>()

    private let _tagManager = ID3TagManager()
    private let _playbackHandler = SystemPlaybackHandler()
    private let _audioPlayerService: PAudioPlayerService = AudioPlayerService.shared
    private let _shareTrackInfoManager = SharedTrackInfoManager()
    private var _disposeBag: Set<AnyCancellable> = Set()

    static let shared = PlaybackManager()
    private init() {
        playlist.currentTrack.sink { [weak self] in
            Logger.shared.log("PlayerManager: currentTrack \($0?.title ?? "")")
            guard let self = self, let track = $0 else { return }
            self.updateInfo(forTrack: track)
        }.store(in: &_disposeBag)

        _audioPlayerService.state.sink {
            Logger.shared.log("PlayerManager: sink state - \($0)")
        }.store(in: &_disposeBag)

        _audioPlayerService.timing.sink { [weak self] in
            self?._shareTrackInfoManager.updateTiming(current: $0.0, total: $0.1)
        }.store(in: &_disposeBag)

        _audioPlayerService.finishPlaying.sink { [weak self] _ in
            self?.playNext()
        }.store(in: &_disposeBag)
    }

    private func updateInfo(forTrack track: MusicTrackModel) {
        let trackTags = _tagManager.tags(forFile: track.url.path, fileName: track.title)
        trackInfo.send((trackTags.title, trackTags.artist, trackTags.picture))
        _shareTrackInfoManager.updateInfo(with: trackTags)
    }

    func prepare() {
        _playbackHandler.setup(withPlayer: self)
        playlist.loadTracks()
        playlist.restoreCurrentTrack()
    }

    func search(text: String?) {
        playlist.search(text: text)
    }
    func removeTrack(atIndexPath indexPath: IndexPath) {
        let nextTrack = playlist.nextTrack()
        playlist.removeItem(atIndexPath: indexPath)
        if let nextTrack = nextTrack {
            playTrack(nextTrack)
        }
    }
    func setTime(_ time: Double) {
        _audioPlayerService.setTime(time)
    }

}

extension PlaybackManager: PlayerManagerProtocol {

    func playTrack(_ track: MusicTrackModel) {
        _audioPlayerService.playTrack(track)
        playlist.setCurrentTrack(track: track)
        updateInfo(forTrack: track)
    }
    func play(fade: Bool = false) {
        Logger.shared.log("\(#function)")

        switch state.value {
        case .paused: Logger.shared.log("\(#function) State: .paused")
        case .playing: Logger.shared.log("\(#function) State: .playing")
        case .stoped: Logger.shared.log("\(#function) State: .stoped")
        }

        if state.value == .paused {
            Logger.shared.log("PM \(#function) continue playing")
            _audioPlayerService.play(fade: fade)
        } else {
            Logger.shared.log("PM \(#function) getting track")
            if let track = playlist.currentTrack.value {
                Logger.shared.log("PM \(#function) play track")
                playTrack(track)
            } else {
                playNext()
            }
        }
    }
    func pause() {
        Logger.shared.log("PlayerManager: \(#function)")
        _audioPlayerService.pause()
    }
    func playPause() {
        Logger.shared.log("PlayerManager: \(#function)")
        switch state.value {
        case .paused, .stoped: play()
        case .playing: pause()
        }
    }
    func playNext() {
        Logger.shared.log("PlayerManager: playNext()")
        guard let track = playlist.nextTrack() else { return }
        Logger.shared.log("PlayerManager: track.url = \(track.url.lastPathComponent)")
        playTrack(track)
    }
    func playPrev() {
        if timings.value.current > 5 {
            _audioPlayerService.setTime(0)
            return
        }
        guard let track = playlist.prevTrack() else { return }
        playTrack(track)
    }
    func removeCurrentTrack() {
        let nextTrack = playlist.nextTrack()
        playlist.removeCurrentTrack()
        if let nextTrack = nextTrack {
            playTrack(nextTrack)
        }
    }
}
