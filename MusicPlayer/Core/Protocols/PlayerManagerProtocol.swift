//
//  PlayerManagerProtocol.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import UIKit

protocol PlayerManagerProtocol {
//    func playTrack(atIndexPath indexPath: IndexPath)
    func playTrack(_ track: MusicTrackModel)
    func play(fade: Bool)
    func pause()
    func playPause()
    func playNext()
    func playPrev()
    func setTime(_ time: Double)
    func removeCurrentTrack()
}
