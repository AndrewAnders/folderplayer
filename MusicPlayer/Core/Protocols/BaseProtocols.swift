//
//  BaseProtocols.swift
//  MusicPlayer
//
//  Created by Savitskiy on 01.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import UIKit

protocol PViewModel {
    var coordinator: Coordinator { get }
    init(coordinator: Coordinator)
}

class BaseVC<T: PViewModel>: UIViewController, Storyborded {
    var viewModel: T?
}
