//
//  PAudioPlayerService.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import Combine

protocol PAudioPlayerService {
    func set(state newState: Playback)
    func setTime(_ time: Double)
    func playTrack(_ track: MusicTrackModel)
    func play(fade: Bool)
    func pause()

    var state: CurrentValueSubject<Playback, Never> { get }
    var timing: CurrentValueSubject<(current: Double, total: Double), Never> { get }
    var finishPlaying: CurrentValueSubject<Void, Never> { get }

}
