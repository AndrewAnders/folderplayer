//
//  PPlaylistManager.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

import UIKit
import Combine

protocol PPlaylistManager {
//    var trackIndexPath: IndexPath { get }
    var currentTrack: CurrentValueSubject<MusicTrackModel?, Never> { get }
    var shuffle: CurrentValueSubject<Bool, Never> { get }
    func loadTracks()
    func restoreCurrentTrack()
    func nextTrack() -> MusicTrackModel?
    func prevTrack() -> MusicTrackModel?
    func indexPath(forTrack track: MusicTrackModel) -> IndexPath
    func search(text: String?)
    func set(shuffle: Bool)
    func removeCurrentTrack()
    func removeItem(atIndexPath indexPath: IndexPath)
    func setCurrentTrack(track: MusicTrackModel)
    func track(atIndexPath indexPath: IndexPath) -> MusicTrackModel?
    var folders: [Folder] { get }
}
