//
//  AudioPlayerService.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation
import AVFoundation
import Combine

class AudioPlayerService: NSObject, PAudioPlayerService {

    // MARK: - PAudioPlayerService
    let state = CurrentValueSubject<Playback, Never>(.stoped)
    let timing: CurrentValueSubject<(current: Double, total: Double), Never> = .init((current: 0, total: 0))
    let finishPlaying = CurrentValueSubject<Void, Never>(())

    // MARK: - Private
    private var _avAudioPlayer: AVAudioPlayer?
    private var _timer: Timer?

    static let shared: PAudioPlayerService = AudioPlayerService()

    private override init() {}
    deinit {
        Logger.shared.log("🛑 AudioPlayerService deinit")
        if state.value != .stoped { stop() }
    }

    func set(state: Playback) {
        self.state.send(state)
    }
    func setTime(_ time: Double) {
        guard let player = self._avAudioPlayer else { return }
        player.currentTime = time
    }

    func playTrack(_ track: MusicTrackModel) {
        Logger.shared.log("PS \(#function)")
        _avAudioPlayer?.stop()
        _avAudioPlayer = nil
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            _avAudioPlayer = try AVAudioPlayer(contentsOf: track.url, fileTypeHint: AVFileType.mp3.rawValue)
            _avAudioPlayer?.prepareToPlay()
            _avAudioPlayer?.delegate = self
        } catch let error {
            print(error.localizedDescription)
        }
        play()
    }

    func play(fade: Bool = false) {
        Logger.shared.log("PS \(#function)")
        guard let player = _avAudioPlayer else { return }
        if fade {
            player.setVolume(0.5, fadeDuration: 0)
            player.setVolume(1, fadeDuration: 0.5)
        }
        player.play()
        state.send(.playing)
        timerStart()
    }
    func pause() {
        Logger.shared.log("PS \(#function)")
        _avAudioPlayer?.pause()
        state.send(.paused)
        timerStop()
    }
    func stop() {
        Logger.shared.log("PS \(#function)")
        _avAudioPlayer?.stop()
        state.send(.stoped)
        timerStop()
    }

    // MARK: - Timer
    func timerStart() {
        _timer?.invalidate()
        guard let player = self._avAudioPlayer else { return }
        timing.send((current: player.currentTime, total: player.duration))
        _timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] _ in
            self?.timing.send((current: player.currentTime, total: player.duration))
        })
    }
    func timerStop() {
        _timer?.invalidate()
        _timer = nil
    }
}

// MARK: - AVAudioPlayerDelegate
extension AudioPlayerService: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        Logger.shared.log("PlayerManager: finishPlaying")
        finishPlaying.send(())
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        Logger.shared.log("\(#function)")
    }
    func audioPlayerBeginInterruption(_ player: AVAudioPlayer) {
        Logger.shared.log("\(#function)")
        PlaybackManager.shared.pause()
    }
    func audioPlayerEndInterruption(_ player: AVAudioPlayer, withOptions flags: Int) {
        Logger.shared.log("\(#function)")
        PlaybackManager.shared.play(fade: true)
    }
}
