//
//  MusicFileManager.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 16.04.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import Foundation

protocol PMusicFileManager {
    func getMusicDictionary() -> [String: [URL]]
    func remove(file url: URL)
    func exists(file path: String) -> Bool
    func getShortPath(url: URL) -> String
}

class MusicFileManager: PMusicFileManager {
    private var _musicDictionary: [String: [URL]] = [:]
    private let _manager = FileManager.default
    private var _documentDir: URL?

    // MARK: - Public methods
    func getMusicDictionary() -> [String: [URL]] {
        _musicDictionary.removeAll()
        if let directoryList = getFilesInFolder(getDocumentsDirectory()) {
            _musicDictionary[getDocumentsDirectory().fileName()] = directoryList
        }
        return _musicDictionary
    }

    func remove(file url: URL) {
        try? _manager.removeItem(at: url)
    }
    func exists(file shortPath: String) -> Bool {
        let path = "\(getDocumentsDirectory().path)\(shortPath)"
        return _manager.fileExists(atPath: path)
    }
    func getShortPath(url: URL) -> String {
        let path = url.path
        let dirPath = getDocumentsDirectory().path
        let range = path.range(of: dirPath)
        guard let range = range else { return path }
        let substring = String(path[range.upperBound...])
        return substring
//        return path.substring(from: range.upperBound)
    }

    // MARK: - Private methods
    private func getDocumentsDirectory() -> URL {
        if let docDir = _documentDir { return docDir }
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        let documentsDirectory = paths[0]
        debugPrint("documentsDirectory: \(documentsDirectory)")
        _documentDir = URL(fileURLWithPath: documentsDirectory)
        return _documentDir!
    }

    private func getFilesInFolder(_ folderURL: URL) -> [URL]? {
        var filesList = [URL]()
        var directoryContent: [URL]?

        do {
            directoryContent = try _manager.contentsOfDirectory(at: folderURL,
                                                               includingPropertiesForKeys: nil,
                                                               options: .skipsHiddenFiles)
        } catch { return nil }
        guard let files = directoryContent else { return nil }
        files.forEach { fileUrl in
            if fileUrl.fileExtension() == "mp3" {
                filesList.append(fileUrl)
            } else if fileUrl.isDirectory {
                let directoryUrl = fileUrl
                if let directoryList = getFilesInFolder(directoryUrl) {
                    _musicDictionary[directoryUrl.fileName()] = directoryList
                }
            }
        }
        return filesList.count > 0 ? filesList : nil
    }
}
