//
//  Coordinator.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 10.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

import UIKit

class Coordinator {
    let window: UIWindow
    let rootViewController: UINavigationController

    init(window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
    }

    func start() {
        window.rootViewController = rootViewController
        showPlayback()
        window.makeKeyAndVisible()
    }

    func showPlayback() {
        let playbackView = PlaybackView.instantiate()
        let viewModel = PlaybackViewModel(coordinator: self)
        playbackView.viewModel = viewModel
        rootViewController.pushViewController(playbackView, animated: true)
    }

    func showPlaylist() {
        let view = PlaylistView.instantiate()
        view.viewModel = PlaylistViewModel(coordinator: self)
        rootViewController.pushViewController(view, animated: true)
    }

    func showLog() {
        let view = LogViewController.instantiate()
        rootViewController.pushViewController(view, animated: true)
    }

}
