//
//  Playback.swift
//  MusicPlayer
//
//  Created by Savitskiy on 04.01.2021.
//  Copyright © 2021 com.adsv. All rights reserved.
//

enum Playback {
    case playing
    case paused
    case stoped
}
