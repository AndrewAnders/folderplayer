//
//  Constants.swift
//  MusicPlayer
//
//  Created by Andrey Savitskiy on 09.05.2020.
//  Copyright © 2020 com.adsv. All rights reserved.
//

struct Constants {
    static let RootDirectoryName = "Root"
    static let Logging = true
}
